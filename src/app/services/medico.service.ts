import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

export enum SearchType {
  all = "",
  nome = "nome",
  cidade = "cidade"
}

@Injectable({
  providedIn: 'root'
})
export class MedicoService {
  urlMedico = "UrlDaAPI";
  urlCidade = "UrlDaAPI";
  urlDetalhes = "UrlDaAPI";

  constructor(private http: HttpClient) { }

  getEspecialista(nome: string, cidade: string){
    return this.http.get(`${this.urlMedico}/${nome}/${cidade}/`)
  }

  getCidade(){
    return this.http.get(`${this.urlCidade}`);
  }

  getDetalhesEspecialista(id){
    return this.http.get(`${this.urlDetalhes}/${id}`)
  }
}



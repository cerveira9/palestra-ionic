import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ArtigoService {
  urlArtigoAtivo = "UrlDaAPI";

  constructor(private http: HttpClient) { }

  getArtigos(): Observable<any>{
    return this.http.get(`${this.urlArtigoAtivo}`).pipe(
      map(results => {
        console.log("RESULTS FROM PIPE: ", results);
        return results;
      }) 
      );
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class NoticiaService {
  urlSliderAtivo = "UrlDaAPI";

  constructor(private http: HttpClient) { }

  getDadosNoticias(): Observable<any>{
    return this.http.get(`${this.urlSliderAtivo}`).pipe(
      map(results => {
        console.log("RESULTS FROM PIPE: ", results);
        return results;
      }) 
      );
  }
}

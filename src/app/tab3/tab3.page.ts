import { Component } from '@angular/core';
import { MedicoService, SearchType } from 'src/app/services/medico.service';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  cidades: any;
  medicos: any;
  busca: string = "";
  type: SearchType = SearchType.all;

  constructor(private medicoService: MedicoService) {}

  ngOnInit() {
    this.medicoService.getCidade().subscribe(data => {
      this.cidades = data;
    });
  }

  searchChanged() {
    this.medicoService
      .getEspecialista(this.busca, this.type)
      .subscribe(data => {
        this.medicos = data;
      });
  }
}

import { Component } from '@angular/core';
import { ArtigoService } from "src/app/services/artigo.service";
import { Observable } from "rxjs";

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  artigos: Observable<any>;
  artigosGet: Observable<any>;

  constructor(private artigoService: ArtigoService) {
    this.getDados();
   }

  getDados() {
    this.artigos = this.artigoService.getArtigos();

    this.artigos.subscribe(res => {
      this.artigosGet = res;
    })
  }

}

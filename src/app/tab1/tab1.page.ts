import { Component } from '@angular/core';
import { NoticiaService } from "../services/noticia.service";
import { Observable } from "rxjs";

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  noticias: Observable<any>;
  noticiasGet: Observable<any>;

  constructor(private noticiaService: NoticiaService) { 
    this.getDados();
  }

  getDados() {
    this.noticias = this.noticiaService.getDadosNoticias();

    this.noticias.subscribe(res => {      
      this.noticiasGet = res;
    })
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetalhesEspecialistaPage } from './detalhes-especialista.page';

const routes: Routes = [
  {
    path: '',
    component: DetalhesEspecialistaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetalhesEspecialistaPage]
})
export class DetalhesEspecialistaPageModule {}

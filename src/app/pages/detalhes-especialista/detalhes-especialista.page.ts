import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MedicoService } from 'src/app/services/medico.service';

@Component({
  selector: 'app-detalhes-especialista',
  templateUrl: './detalhes-especialista.page.html',
  styleUrls: ['./detalhes-especialista.page.scss'],
})
export class DetalhesEspecialistaPage implements OnInit {
  detail = null;

  constructor(private activatedRoute: ActivatedRoute, private medicoService: MedicoService) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');

    this.medicoService.getDetalhesEspecialista(id).subscribe(result => {
      console.log('details: ', result);
      this.detail = result;
    })
  }
}
